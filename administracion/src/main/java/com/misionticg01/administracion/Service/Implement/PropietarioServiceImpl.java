 
package com.misionticg01.administracion.Service.Implement;

import com.misionticg01.administracion.Dao.PropietarioDao;
import com.misionticg01.administracion.Models.Propietario;
import com.misionticg01.administracion.Service.PropietarioService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service; 
import org.springframework.transaction.annotation.Transactional;

@Service
public class PropietarioServiceImpl implements PropietarioService{
    @Autowired
    private PropietarioDao propietarioDao;

    @Override
    @Transactional(readOnly=false)
    public Propietario save(Propietario propietario) {
        
        return propietarioDao.save(propietario);
     }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        
        propietarioDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Propietario findById(Integer id) {
        
        return propietarioDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly=true)
    public List<Propietario> findByAll() {
        
        return (List<Propietario>) propietarioDao.findAll();
        
    }
    
}

