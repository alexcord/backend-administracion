 
package com.misionticg01.administracion.Service.Implement;

import com.misionticg01.administracion.Dao.ResidenciaDao;
import com.misionticg01.administracion.Models.Residencia;
import com.misionticg01.administracion.Service.ResidenciaService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service; 
import org.springframework.transaction.annotation.Transactional;

@Service
public class ResidenciaServiceImpl implements ResidenciaService{
    @Autowired
    private ResidenciaDao residenciaDao;

    @Override
    @Transactional(readOnly=false)
    public Residencia save(Residencia residencia) {
        
        return residenciaDao.save(residencia);
     }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        
        residenciaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Residencia findById(Integer id) {
        
        return residenciaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly=true)
    public List<Residencia> findByAll() {
        
        return (List<Residencia>) residenciaDao.findAll();
        
    }
    
}

