 
package com.misionticg01.administracion.Service.Implement;

import com.misionticg01.administracion.Dao.FacturaDao;
import com.misionticg01.administracion.Models.Factura;
import com.misionticg01.administracion.Service.FacturaService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service; 
import org.springframework.transaction.annotation.Transactional;

@Service
public class FacturaServiceImpl implements FacturaService{
    @Autowired
    private FacturaDao facturaDao;

    @Override
    @Transactional(readOnly=false)
    public Factura save(Factura factura) {
        
        return facturaDao.save(factura);
     }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        
        facturaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Factura findById(Integer id) {
        
        return facturaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly=true)
    public List<Factura> findByAll() {
        
        return (List<Factura>) facturaDao.findAll();
        
    }
    
}

