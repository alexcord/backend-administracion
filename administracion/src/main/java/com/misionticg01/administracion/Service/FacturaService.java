 
package com.misionticg01.administracion.Service;

import com.misionticg01.administracion.Models.Factura;
import java.util.List;

public interface FacturaService {
    
    public Factura save(Factura factura);
    public void delete(Integer id);
    public Factura findById(Integer id);
    public List<Factura> findByAll();
    
}
