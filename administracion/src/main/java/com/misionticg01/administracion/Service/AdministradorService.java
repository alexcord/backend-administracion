 
package com.misionticg01.administracion.Service;

import com.misionticg01.administracion.Models.Administrador;
import java.util.List;
 
public interface AdministradorService {
    
    public Administrador save(Administrador administrador);
    public void delete(Integer id);
    public Administrador findById(Integer id);
    public List<Administrador> findByAll();
    
}
