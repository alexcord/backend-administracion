 
package com.misionticg01.administracion.Service.Implement;

import com.misionticg01.administracion.Dao.AdministradorDao;
import com.misionticg01.administracion.Models.Administrador;
import com.misionticg01.administracion.Service.AdministradorService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service; 
import org.springframework.transaction.annotation.Transactional;

@Service
public class AdministradorServiceImpl implements AdministradorService{
    @Autowired
    private AdministradorDao administradorDao;

    @Override
    @Transactional(readOnly=false)
    public Administrador save(Administrador administrador) {
        
        return administradorDao.save(administrador);
     }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        
        administradorDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Administrador findById(Integer id) {
        
        return administradorDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly=true)
    public List<Administrador> findByAll() {
        
        return (List<Administrador>) administradorDao.findAll();
        
    }
    
}
