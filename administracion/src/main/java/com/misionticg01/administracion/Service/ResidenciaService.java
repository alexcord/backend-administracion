 
package com.misionticg01.administracion.Service;

import com.misionticg01.administracion.Models.Residencia;
import java.util.List; 

public interface ResidenciaService {
    
    public Residencia save(Residencia residencia);
    public void delete(Integer id);
    public Residencia findById(Integer id);
    public List<Residencia> findByAll();
    
}
