 
package com.misionticg01.administracion.Service;

import com.misionticg01.administracion.Models.Propietario;
import java.util.List; 

public interface PropietarioService {
    
    public Propietario save(Propietario propietario);
    public void delete(Integer id);
    public Propietario findById(Integer id);
    public List<Propietario> findByAll();
}
