 
package com.misionticg01.administracion.Service;

import com.misionticg01.administracion.Models.Conjunto;
import java.util.List;

public interface ConjuntoService {
    
    public Conjunto save(Conjunto conjunto);
    public void delete(Integer id);
    public Conjunto findById(Integer id);
    public List<Conjunto> findByAll();
    
}
