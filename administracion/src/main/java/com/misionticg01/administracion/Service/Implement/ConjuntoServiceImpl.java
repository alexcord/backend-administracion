 
package com.misionticg01.administracion.Service.Implement;

import com.misionticg01.administracion.Dao.ConjuntoDao;
import com.misionticg01.administracion.Models.Conjunto;
import com.misionticg01.administracion.Service.ConjuntoService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service; 
import org.springframework.transaction.annotation.Transactional;

@Service
public class ConjuntoServiceImpl implements ConjuntoService{
    @Autowired
    private ConjuntoDao conjuntoDao;

    @Override
    @Transactional(readOnly=false)
    public Conjunto save(Conjunto conjunto) {
        
        return conjuntoDao.save(conjunto);
     }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        
        conjuntoDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Conjunto findById(Integer id) {
        
        return conjuntoDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly=true)
    public List<Conjunto> findByAll() {
        
        return (List<Conjunto>) conjuntoDao.findAll();
        
    }
     
    
}
