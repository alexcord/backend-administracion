 
package com.misionticg01.administracion.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator; 

@Table
@Entity(name="conjunto")
public class Conjunto implements Serializable{
    @Id
    @Column(name="id_conjunto")
    private int id_conjunto;
    @Column(name="nombre_conjunto")
    private String nombre_conjunto;
    @Column(name="dir_conjunto")
    private String  dir_conjunto;
    
    @ManyToOne
    @JoinColumn(name="cedula_admin")
    private Administrador administrador;

    public Conjunto(int id_conjunto, String nombre_conjunto, String dir_conjunto, Administrador administrador ) {
        this.id_conjunto = id_conjunto;
        this.nombre_conjunto = nombre_conjunto;
        this.dir_conjunto = dir_conjunto;
        this.administrador=administrador;
    }

    public Conjunto() {
    }

    public int getId_conjunto() {
        return id_conjunto;
    }

    public void setId_conjunto(int id_conjunto) {
        this.id_conjunto = id_conjunto;
    }

    public String getNombre_conjunto() {
        return nombre_conjunto;
    }

    public void setNombre_conjunto(String nombre_conjunto) {
        this.nombre_conjunto = nombre_conjunto;
    }

    public String getDir_conjunto() {
        return dir_conjunto;
    }

    public void setDir_conjunto(String dir_conjunto) {
        this.dir_conjunto = dir_conjunto;
    }

    public Administrador getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Administrador administrador) {
        this.administrador = administrador;
    }

    
}
