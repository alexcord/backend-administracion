 
package com.misionticg01.administracion.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


@Table
@Entity(name="administrador")
public class Administrador implements Serializable{
    @Id
    @Column(name="cedula_admin")
    private int cedula_admin;
    @Column(name="nombre_admin")
    private String nombre_admin;
    @Column(name="telefono_admin")
    private int telefono_admin;
    @Column(name="correo_admin")
    private String correo_admin;

    public Administrador(int cedula_admin, String nombre_admin, int telefono_admin, String correo_admin) {
        this.cedula_admin = cedula_admin;
        this.nombre_admin = nombre_admin;
        this.telefono_admin = telefono_admin;
        this.correo_admin = correo_admin;
    }

    public Administrador() {
    }

    public int getCedula_admin() {
        return cedula_admin;
    }

    public void setCedula_admin(int cedula_admin) {
        this.cedula_admin = cedula_admin;
    }

    public String getNombre_admin() {
        return nombre_admin;
    }

    public void setNombre_admin(String nombre_admin) {
        this.nombre_admin = nombre_admin;
    }

    public int getTelefono_admin() {
        return telefono_admin;
    }

    public void setTelefono_admin(int telefono_admin) {
        this.telefono_admin = telefono_admin;
    }

    public String getCorreo_admin() {
        return correo_admin;
    }

    public void setCorreo_admin(String correo_admin) {
        this.correo_admin = correo_admin;
    }
    
    
    
    
    
    
}













