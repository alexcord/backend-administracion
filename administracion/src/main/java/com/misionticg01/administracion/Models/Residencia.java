 
package com.misionticg01.administracion.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator; 
 
@Table
@Entity(name="residencia")
public class Residencia implements Serializable{
    @Id
    @Column(name="id_residencia")
    private int id_residencia;
    @Column(name="dirección_residencia")
    private String  dirección_residencia;
    
    @ManyToOne
    @JoinColumn(name="id_conjunto")
    private Conjunto conjunto;
    
    @ManyToOne
    @JoinColumn(name="cedula_propietario")
    private Propietario propietario;

    public Residencia(int id_residencia, String dirección_residencia, Conjunto conjunto, Propietario propietario) {
        this.id_residencia = id_residencia;
        this.dirección_residencia = dirección_residencia;
        this.conjunto = conjunto;
        this.propietario = propietario;
    }

    public Residencia() {
    }

    public int getId_residencia() {
        return id_residencia;
    }

    public void setId_residencia(int id_residencia) {
        this.id_residencia = id_residencia;
    }

    public String getDirección_residencia() {
        return dirección_residencia;
    }

    public void setDirección_residencia(String dirección_residencia) {
        this.dirección_residencia = dirección_residencia;
    }

    public Conjunto getConjunto() {
        return conjunto;
    }

    public void setConjunto(Conjunto conjunto) {
        this.conjunto = conjunto;
    }

    public Propietario getPropietario() {
        return propietario;
    }

    public void setPropietario(Propietario propietario) {
        this.propietario = propietario;
    }

     
    
    
    
}
