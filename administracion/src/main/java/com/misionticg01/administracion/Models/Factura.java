 
package com.misionticg01.administracion.Models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator; 

@Table 
@Entity(name="factura")
public class Factura implements Serializable{
    @Id
    @Column(name="id_factura")
    private int id_factura;
    @Column(name=" valor_factura")
    private double  valor_factura;
    @Column(name=" fecha_pago")
    private Date  fecha_pago;
    @Column(name=" fecha_pagado")
    private Date  fecha_pagado;
    
    @ManyToOne
    @JoinColumn(name="cedula_propietario")
    private Propietario propietario;
    
    @ManyToOne
    @JoinColumn(name="id_residencia")
    private Residencia residencia;

    public Factura(int id_factura, double valor_factura, Date fecha_pago, Date fecha_pagado, Propietario propietario, Residencia residencia) {
        this.id_factura = id_factura;
        this.valor_factura = valor_factura;
        this.fecha_pago = fecha_pago;
        this.fecha_pagado = fecha_pagado;
        this.propietario = propietario;
        this.residencia = residencia;
    }

    public Factura() {
    }

    public int getId_factura() {
        return id_factura;
    }

    public void setId_factura(int id_factura) {
        this.id_factura = id_factura;
    }

    public double getValor_factura() {
        return valor_factura;
    }

    public void setValor_factura(double valor_factura) {
        this.valor_factura = valor_factura;
    }

    public Date getFecha_pago() {
        return fecha_pago;
    }

    public void setFecha_pago(Date fecha_pago) {
        this.fecha_pago = fecha_pago;
    }

    public Date getFecha_pagado() {
        return fecha_pagado;
    }

    public void setFecha_pagado(Date fecha_pagado) {
        this.fecha_pagado = fecha_pagado;
    }

    public Propietario getPropietario() {
        return propietario;
    }

    public void setPropietario(Propietario propietario) {
        this.propietario = propietario;
    }

    public Residencia getResidencia() {
        return residencia;
    }

    public void setResidencia(Residencia residencia) {
        this.residencia = residencia;
    }

    
    
    
    
}
