 
package com.misionticg01.administracion.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator; 

@Table
@Entity(name="propietario")
public class Propietario implements Serializable{
    @Id
    @Column(name="cedula_propietario")
    private int cedula_propietario;
    @Column(name="nombre_propietario")
    private String nombre_propietario;
    @Column(name="telefono_propietario")
    private int telefono_propietario;
    @Column(name="correo_propietario")
    private String correo_propietario;

    public Propietario(int cedula_propietario, String nombre_propietario, int telefono_propietario, String correo_propietario) {
        this.cedula_propietario = cedula_propietario;
        this.nombre_propietario = nombre_propietario;
        this.telefono_propietario = telefono_propietario;
        this.correo_propietario = correo_propietario;
    }

    public Propietario() {
    }

    public int getCedula_propietario() {
        return cedula_propietario;
    }

    public void setCedula_propietario(int cedula_propietario) {
        this.cedula_propietario = cedula_propietario;
    }

    public String getNombre_propietario() {
        return nombre_propietario;
    }

    public void setNombre_propietario(String nombre_propietario) {
        this.nombre_propietario = nombre_propietario;
    }

    public int getTelefono_propietario() {
        return telefono_propietario;
    }

    public void setTelefono_propietario(int telefono_propietario) {
        this.telefono_propietario = telefono_propietario;
    }

    public String getCorreo_propietario() {
        return correo_propietario;
    }

    public void setCorreo_propietario(String correo_propietario) {
        this.correo_propietario = correo_propietario;
    }
    
    
}
