 
package com.misionticg01.administracion.Dao;

import com.misionticg01.administracion.Models.Administrador;
import org.springframework.data.repository.CrudRepository;


public interface AdministradorDao extends CrudRepository<Administrador, Integer> {
    
}
