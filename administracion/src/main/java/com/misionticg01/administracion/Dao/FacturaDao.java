 
package com.misionticg01.administracion.Dao;

import com.misionticg01.administracion.Models.Factura;
import org.springframework.data.repository.CrudRepository;  

public interface FacturaDao extends CrudRepository<Factura, Integer>{
    
}
