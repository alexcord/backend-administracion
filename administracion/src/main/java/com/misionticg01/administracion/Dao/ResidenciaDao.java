 
package com.misionticg01.administracion.Dao;

import com.misionticg01.administracion.Models.Residencia;
import org.springframework.data.repository.CrudRepository;  

public interface ResidenciaDao extends CrudRepository<Residencia, Integer>{
    
}
