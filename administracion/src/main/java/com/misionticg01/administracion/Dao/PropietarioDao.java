 
package com.misionticg01.administracion.Dao;

import com.misionticg01.administracion.Models.Propietario;
import org.springframework.data.repository.CrudRepository; 

public interface PropietarioDao extends CrudRepository<Propietario, Integer> {
    
}
