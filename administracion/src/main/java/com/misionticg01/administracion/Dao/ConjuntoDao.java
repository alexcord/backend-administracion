 
package com.misionticg01.administracion.Dao;

import com.misionticg01.administracion.Models.Conjunto;
import org.springframework.data.repository.CrudRepository; 

public interface ConjuntoDao extends CrudRepository<Conjunto, Integer>  {
    
}
