 
package com.misionticg01.administracion.Controller;

import com.misionticg01.administracion.Models.Administrador;
import com.misionticg01.administracion.Service.AdministradorService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/administrador") 
public class AdministradorController {
    @Autowired
    private AdministradorService administradorService;
    
     @PostMapping(value="/")
    public ResponseEntity<Administrador> agregar(@RequestBody Administrador administrador){        
        Administrador obj = administradorService.save(administrador);
        return new ResponseEntity<>(obj, HttpStatus.OK);     
    }
   
    @DeleteMapping(value="/{id}") 
    public ResponseEntity<Administrador> eliminar(@PathVariable Integer id){ 
        Administrador obj = administradorService.findById(id); 
        if(obj!=null) 
            administradorService.delete(id); 
        else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }
    
    @PutMapping(value="/") 
    public ResponseEntity<Administrador> editar(@RequestBody Administrador administrador){ 
        Administrador obj = administradorService.findById(administrador.getCedula_admin()); 
        if(obj!=null) {
            obj.setNombre_admin(administrador.getNombre_admin());
            obj.setCorreo_admin(administrador.getCorreo_admin());
            obj.setTelefono_admin(administrador.getTelefono_admin());
            administradorService.save(obj); 
        } 
        else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }
    
    @GetMapping("/list")
    public List<Administrador> consultarTodo(){
        return administradorService.findByAll(); 
    }
    
    @GetMapping("/list/{id}") 
    public Administrador consultaPorId(@PathVariable Integer id){ 
        return administradorService.findById(id); 
    }
}
