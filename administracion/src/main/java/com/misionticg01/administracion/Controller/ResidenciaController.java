
package com.misionticg01.administracion.Controller;

import com.misionticg01.administracion.Models.Residencia;
import com.misionticg01.administracion.Service.ResidenciaService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/residencia") 
public class ResidenciaController {
    @Autowired
    private ResidenciaService residenciaService;
    
     @PostMapping(value="/")
    public ResponseEntity<Residencia> agregar(@RequestBody Residencia residencia){        
        Residencia obj = residenciaService.save(residencia);
        return new ResponseEntity<>(obj, HttpStatus.OK);     
    }
   
    @DeleteMapping(value="/{id}") 
    public ResponseEntity<Residencia> eliminar(@PathVariable Integer id){ 
        Residencia obj = residenciaService.findById(id); 
        if(obj!=null) 
            residenciaService.delete(id); 
        else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }
    
    @PutMapping(value="/") 
    public ResponseEntity<Residencia> editar(@RequestBody Residencia residencia){ 
        Residencia obj = residenciaService.findById(residencia.getId_residencia());
        if(obj!=null) {
            obj.setDirección_residencia(residencia.getDirección_residencia());
            obj.setConjunto(residencia.getConjunto());
            obj.setPropietario(residencia.getPropietario());
            residenciaService.save(obj); 
        } 
        else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }
    
    @GetMapping("/list")
    public List<Residencia> consultarTodo(){
        return residenciaService.findByAll(); 
    }
    
    @GetMapping("/list/{id}") 
    public Residencia consultaPorId(@PathVariable Integer id){ 
        return residenciaService.findById(id); 
    }
}

