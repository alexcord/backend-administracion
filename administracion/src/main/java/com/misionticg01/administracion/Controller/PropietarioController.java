
package com.misionticg01.administracion.Controller;

import com.misionticg01.administracion.Models.Propietario;
import com.misionticg01.administracion.Service.PropietarioService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/propietario") 
public class PropietarioController {
    @Autowired
    private PropietarioService propietarioService;
    
     @PostMapping(value="/")
    public ResponseEntity<Propietario> agregar(@RequestBody Propietario propietario){        
        Propietario obj = propietarioService.save(propietario);
        return new ResponseEntity<>(obj, HttpStatus.OK);     
    }
   
    @DeleteMapping(value="/{id}") 
    public ResponseEntity<Propietario> eliminar(@PathVariable Integer id){ 
        Propietario obj = propietarioService.findById(id); 
        if(obj!=null) 
            propietarioService.delete(id); 
        else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }
    
    @PutMapping(value="/") 
    public ResponseEntity<Propietario> editar(@RequestBody Propietario propietario){ 
        Propietario obj = propietarioService.findById(propietario.getCedula_propietario());
        if(obj!=null) {
            obj.setNombre_propietario(propietario.getNombre_propietario());
            obj.setTelefono_propietario(propietario.getTelefono_propietario());
            obj.setCorreo_propietario(propietario.getCorreo_propietario());
            propietarioService.save(obj); 
        } 
        else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }
    
    @GetMapping("/list")
    public List<Propietario> consultarTodo(){
        return propietarioService.findByAll(); 
    }
    
    @GetMapping("/list/{id}") 
    public Propietario consultaPorId(@PathVariable Integer id){ 
        return propietarioService.findById(id); 
    }
}

